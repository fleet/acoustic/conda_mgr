# -*- coding: utf-8 -*-
"""
This module create all environments defined for the pyat project
return 0 if everything went OK or the error code value otherwise
"""
import argparse
import platform
from typing import List

import os
import shutil
import sys
import tempfile


class Env:
    def __init__(self, runner: str = "conda", preview = False):
        """

        :param runner: "conda", "mamba"

        :param preview: run or just print command
        """
        self.runner = runner
        self.conda_setup_directory = self.get_conda_path()
        self.preview = preview

    def get_conda_path(self):
        """
        retrieve anaconda setup directory
        :return:
        """
        outputfile = tempfile.mktemp(suffix="anaconda_path.txt")
        self._process_exec("conda info > " + outputfile)
        # parse the file
        try:
            with open(outputfile, "r") as f:
                content = f.readlines()
                for line in content:
                    if line.find("base environment") >= 0:
                        subparts = line.split(":")
                        rightpart = subparts[1:]
                        rightpart = ":".join(rightpart)
                        dir = rightpart.split("(")[0]
                        conda_setup_directory = dir.strip()
                        break
        except Exception as e:
            print("Unknown error while trying to retrieve conda path, continuing ", e)
            raise e

        return conda_setup_directory

    def has_environment(self, env_name: str):
        """Check if the environment exists"""
        conda_path = self.conda_setup_directory
        dir_env = conda_path + "/envs/" + env_name
        if os.path.exists(dir_env):
            return True
        return False

    def process_exec(self,command: str) -> int:
        print("Executing :" + command)
        if self.preview:
            return
        self._process_exec(command=command)


    def _process_exec(self, command: str) -> int:
        """
        Execute a command.
        exit process if an error occurred otherwise return 0

        """
        # execute command
        exit_code = os.system(command)
        # if linux, retrieve the associated error code
        if not platform.system() == "Windows":
            exit_code = os.WEXITSTATUS(exit_code)
        # if an error occurred, exit all
        if exit_code > 0:
            msg = f"Error while executing command: {command} :  return code {str(exit_code)}"
            print(msg, file=sys.stderr)
            raise Exception(msg)
        # always return 0
        return exit_code

    def _cleanup_env(self, env_name):
        """sometimes conda keep .trash files in environment so try to clean it up"""

        if self.preview:
            print(f"Cleaning environment {env_name}")
            return

        dir_env = self.conda_setup_directory + "/envs/" + env_name
        if os.path.exists(dir_env):
            try:
                tempdir = tempfile.mkdtemp(suffix="old_env")
                shutil.move(dir_env, tempdir)
                print("Moving directory {} to {}".format(dir_env, tempdir))
            except OSError as e:
                # Log error and try to continue
                print("An error occurred {}".format(e))

    @staticmethod
    def _env2yml(env_name):
        """
        Retrieve dev yml file associated with this environment name
        The rule is to have a file with the same name as the env_name, prefixed by "requirements_" and suffixed by the os
        If no file exists in current directory, not suffixed is added
        """

        default_name = f"requirements_{env_name}.yml"
        system_name = f"requirements_{env_name}_{str.lower(platform.system())}.yml"
        if os.path.exists(system_name):
            return system_name
        return default_name

    @staticmethod
    def __yml(env_name, yml_file):
        """If yml_file is None compute its name and return it
        otherwise return yml_file
        """
        return (
            yml_file if yml_file is not None else Env._env2yml(env_name=env_name)
        )

    def remove_env(self, env_name):
        try:
            self.process_exec(f"{self.runner} env remove --name " + env_name)
        except:
            pass
        self._cleanup_env(env_name)

    def create_env(self, env_name: str, yml_file: str = None):
        self.process_exec(
            f"{self.runner} env create -f "
            + self.__yml(env_name, yml_file)
            + " --name "
            + env_name
        )

    def clone_and_update(self, env_name: str, base_env_name: str, yml_file: str = None):
        # clone runtime environment
        self.process_exec(
            f"{self.runner} create -y --name " + env_name + " --clone " + base_env_name
        )
        # update environment with yaml file
        self.process_exec(
            f"{self.runner} env update --file "
            + Env.__yml(env_name, yml_file)
            + " --name "
            + env_name
        )

    def add_develop(self,env_name: str, path_to_add:str):
        """Add to env_name the path_to_add with conda develop module (should be installed previously)
        :param env_name the environment where will be installed the package
        :param path_to_add the path to add as a python package
        """

        self.process_exec(f"{self.runner} run -n {env_name} {self.runner} develop {path_to_add}")

    def clear_all(self,env_names: List[str]):
        for env in env_names:
            self.process_exec(f"{self.runner} env remove --name " + env)
        self.process_exec(f"{self.runner} clean --all --yes ")
        for env in env_names:
            self._cleanup_env(f"{self.runner} env remove --name " + env)


class DevEnv:
    def __init__(self,environment_runtime_name:str,
        environment_test_name:str,
        environment_dev_name:str):
        # environment names as defined in the yml files
        self.environment_runtime_name =environment_runtime_name
        self.environment_test_name = environment_test_name
        self.environment_dev_name = environment_dev_name

    def create_for_runtime(self, env: Env):
        """create environment for runtime targets"""
        # remove old environment
        env.remove_env(env_name=self.environment_runtime_name)
        # create runtime
        env.create_env(env_name=self.environment_runtime_name)
        return 0

    def create_for_test(self, env: Env):
        """create environment for test targets, ie create runtime and test environment"""
        self.create_for_runtime(env)
        # remove old environment
        env.remove_env(self.environment_test_name)
        # clone test environment
        env.clone_and_update(env_name=self.environment_test_name, base_env_name=self.environment_runtime_name)

        return 0

    def create_for_dev(self, env: Env):
        """create environment for development, ie create runtime, test and dev environments"""
        self.create_for_test(env=env)
        # remove old environment
        env.remove_env(self.environment_dev_name)
        # clone test environment
        env.clone_and_update(env_name=self.environment_dev_name, base_env_name=self.environment_test_name)
        return 0

    def _run(self, args, preview):
        env_mgr = Env(preview=preview)
        if args.runner:
            env_mgr.runner = args.runner
        if "runtime" in args.target:
            return self.create_for_runtime(env_mgr)
        if "dev" in args.target:
            return self.create_for_dev(env_mgr)
        if "test" in args.target:
            return self.create_for_test(env_mgr)
        if "clean" in args.target:
            return env_mgr.clean_all(env_mgr)
        print("Unknown target, see help for more information", file=sys.stderr)
        return 1

    def __call__(self, preview = False):
        parser = argparse.ArgumentParser(
            description="recreate the anaconda pyat environment depending on the target platform")
        parser.add_argument("-target", required=True, help="select the environment target (runtime,test,dev,clean)")
        parser.add_argument("-runner", required=False, default="conda", help="choose an installed runner (conda,mamba)")
        # parse arg, exit 2 if not defined
        args = parser.parse_args()
        # execute and interpret argument and exit with returned value
        exit(self._run(args,preview=preview))
