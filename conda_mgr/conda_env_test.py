from conda_mgr.conda_env import Env


def test_preview():
    env=Env(preview=True)
    env.create_env(env_name="test")
    conda_path=env.get_conda_path()
    print(f"Conda path {conda_path}")

    assert  env.has_environment(env_name="test3145666") == False

    env.clone_and_update(env_name="to_be_deleted",base_env_name='test')
