# conda_env

Set of tools allowing to manage anaconda environments for the fleet softwares

This tool is used in subproject to manage conda environment creation, deletion and inheritances

# usage requirements
A anaconda distribution is supposed to be preinstalled

# Compilation and deployement
Compilation of the project (with setuptools and the poml file)
> python -m build

Setup of twine for upload
> conda install twine
>
> twine upload --repository srv_conda_env dist/* --verbose



Note : id of server is defined in ~/.pypirc. An example of a pypirc file is given below :

````
[distutils]
index-servers =
	srv_conda_env

[srv_conda_env]
repository = https://gitlab.ifremer.fr/api/v4/projects/fleet%2Facoustic%2Fconda_mgr/packages/pypi
username = Private-xxx
password = XXXXX
````




